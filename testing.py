import dpkt
import socket, random,time

# echo            = dpkt.icmp.ICMP.Echo()                    # create instance of ICMP Echo
# echo.id         = random.randint(0,0xffff)                 # Here Observe ICMP Packet Max bit length is 16 bits that supports upto 2^16 number
# echo.seq        = random.randint(0,0xffff)                 # Same Logic for Sequence Number.
# #echo.data       = "You Can Put Your Data Optionally Here " # Put Optional Data Here.
# icmp            = dpkt.icmp.ICMP()                         # now create instance of ICMP it self 
# icmp.type       = dpkt.icmp.ICMP_DNS                 # Set Type ICMP Echo that set type : 8 in ICMP field
# icmp.data       = echo                                # set echo instance as data of ICMP packet         
#     
# 
#     
# for i in range(5):
#     s = socket.socket(socket.AF_INET,socket.SOCK_RAW,dpkt.ip.IP_PROTO_ICMP)
#     s.connect(('74.125.239.112',1))
#     sent = s.send(str(icmp))
#     time.sleep(2)
#     print 'sent %d bytes' % sent


UDP_IP="74.125.239.112"
UDP_PORT=50005

message=""
while len(message) <= 16384 :
    message = message + str(len(message)) + " " + 16*"+" + "\n"

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT
print "message:", message, "Length: ", len(message)

sock = socket.socket( socket.AF_INET, # Internet
                      socket.SOCK_DGRAM ) # UDP
sock.sendto( message, (UDP_IP, UDP_PORT) )
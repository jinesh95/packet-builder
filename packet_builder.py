

'''

This is Basic Packet Builder Module.That Generate Various Traffic Packets.
Author : Jinesh Patel
Bug Reporting  : jinesh93@gmail.com

'''

import dpkt
import socket, random,time





def icmp_ping_request(self):
    ''' Packet Structure at High Level
    
    |<----8 bit--->|<--8bit--->|<---------16 bits----------->|
    |--------------------------------------------------------|
    |Type (8 or 0) |  Code (0) |        Checksum             |
    |--------------------------------------------------------|
    |     Identifier           |         Seq. Number         |
    |--------------------------------------------------------|
    |                      Optional Data                     |
    |--------------------------------------------------------|
    
    for ICMP request message Ping Request is Type : 8 and Code : 0
    for ICMP reply message Type is : 0  and Code is :0
    
    '''
    '''Set ICMP Packet Params'''
    self.echo            = dpkt.icmp.ICMP.Echo()                    # create instance of ICMP Echo
    self.echo.id         = random.randint(0,0xffff)                 # Here Observe ICMP Packet Max bit length is 16 bits that supports upto 2^16 number
    self.echo.seq        = random.randint(0,0xffff)                 # Same Logic for Sequence Number.
    self.echo.data       = "You Can Put Your Data Optionally Here " # Put Optional Data Here.
    
    
    self.icmp            = dpkt.icmp.ICMP()                         # now create instance of ICMP it self 
    self.icmp.type       = dpkt.icmp.ICMP_ECHO                      # Set Type ICMP Echo that set type : 8 in ICMP field
    self.icmp.data       = self.echo                                # set echo instance as data of ICMP packet         


# testing Code Written Below for Test This Module

# for i in range(10):
#     s = socket.socket(socket.AF_INET, socket.SOCK_RAW, dpkt.ip.IP_PROTO_ICMP)
#     s.connect(('74.125.239.128', 1))
#     sent = s.send(str(icmp))
#     time.sleep(2)
#     print 'sent %d bytes' % sent


def icmp_ping_reply(self):
    self.echo            = dpkt.icmp.ICMP.Echo()                    # create instance of ICMP Echo
    self.echo.id         = random.randint(0,0xffff)                 # Here Observe ICMP Packet Max bit length is 16 bits that supports upto 2^16 number
    self.echo.seq        = random.randint(0,0xffff)                 # Same Logic for Sequence Number.
    self.echo.data       = "You Can Put Your Data Optionally Here " # Put Optional Data Here.
    
    
    self.icmp            = dpkt.icmp.ICMP()                         # now create instance of ICMP it self 
    self.icmp.type       = dpkt.icmp.ICMP_ECHOREPLY                 # Set Type ICMP Echo that set type : 0 in ICMP field
    self.icmp.data       = self.echo                                # set echo instance as data of ICMP packet         

def icmp_dns_request(self):
    self.echo            = dpkt.icmp.ICMP.Echo()                    # create instance of ICMP Echo
    self.echo.id         = random.randint(0,0xffff)                 # Here Observe ICMP Packet Max bit length is 16 bits that supports upto 2^16 number
    self.echo.seq        = random.randint(0,0xffff)                 # Same Logic for Sequence Number.
    self.echo.data       = "You Can Put Your Data Optionally Here " # Put Optional Data Here.
    
    
    self.icmp            = dpkt.icmp.ICMP()                         # now create instance of ICMP it self 
    self.icmp.type       = dpkt.icmp.ICMP_DNS                       # Set Type ICMP Echo that set type : 0 in ICMP field
    self.icmp.data       = self.echo                                # set echo instance as data of ICMP packet         



